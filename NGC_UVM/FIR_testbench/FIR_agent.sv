// Agent class
// Alberto Reategui

class FIR_agent extends uvm_agent;

	// Macro utility for automation
	`uvm_component_utils(FIR_agent)

	// Declare handles for the driver, monitor and sequencer
	FIR_driver driver;
	FIR_monitor monitor;
	FIR_sequencer sequencer;

	// Agent constructor
	function new(string name="FIR_agent", uvm_component parent);
		super.new(name, parent);
		`uvm_info("AGENT CLASS", "Constructor...", UVM_HIGH)
	endfunction : new

	function void build_phase(uvm_phase phase);
		
		super.build_phase(phase);
		// Create the instances
		sequencer = FIR_sequencer::type_id::create("sequencer", this);
		driver = FIR_driver::type_id::create("driver", this);
		monitor = FIR_monitor::type_id::create("monitor", this);
		`uvm_info("AGENT CLASS", "Entering Build Phase...", UVM_HIGH)

	endfunction: build_phase

	// Creating the connect_phase method
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("AGENT CLASS", "Entering Connect Phase...", UVM_HIGH)
		// Connect the sequencer and driver together
		driver.seq_item_port.connect(sequencer.seq_item_export);
	endfunction : connect_phase

	task run_phase(uvm_phase phase);
		super.run_phase(phase);
	endtask: run_phase

endclass: FIR_agent
