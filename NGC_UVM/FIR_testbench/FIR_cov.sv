
class FIR_cov extends uvm_subscriber #(FIR_sequence_item);

	`uvm_component_utils(FIR_cov)

	// Variable for coverage reported in the module
	real cov;
	
	// Inputs that need to be covered
	bit [31:0] inData = 0;	
  	
	covergroup input_data;
		option.per_instance = 1;

		coverpoint inData {
			bins inData_range[] = {[0:$]};
	  	}

	endgroup : input_data

	// Coverage class constructor
	function new (string name = "FIR_cov", uvm_component parent);

		super.new(name, parent);
		// Create an instance of the covergroup
		input_data = new();	

	endfunction

	function void write (T t);
    
		// Extract the contents of the input data		
		inData = t.inData;
		// Sample the input data
		input_data.sample();

	endfunction : write
	

	function void extract_phase(uvm_phase phase);
		// Get the coverage from the input data
		cov = input_data.get_coverage();
	
	endfunction : extract_phase
	
	function void report_phase(uvm_phase phase);

		// Report the result of the test
		`uvm_info(get_full_name(), $sformatf("Coverage is %d", cov), UVM_NONE);

	endfunction : report_phase

endclass : FIR_cov