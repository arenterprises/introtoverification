// Driver Class
// Alberto Reategui

class FIR_driver extends uvm_driver #(FIR_sequence_item);

        `uvm_component_utils(FIR_driver)
        
	// Declare handles for the 
	virtual FIR_interface vif;
	FIR_sequence_item item;
	
	// Constructor
        function new(string name = "FIR_driver", uvm_component parent);
                super.new(name, parent);
                `uvm_info("DRIVER CLASS", "Constructor for Driver...", UVM_HIGH)
        endfunction: new

	// Build Phase        
        function void build_phase(uvm_phase phase);
                super.build_phase(phase);
		`uvm_info("DRIVER CLASS", "Build Phase for Driver...", UVM_HIGH)
		if(!(uvm_config_db #(virtual FIR_interface)::get(this, "*", "vif", vif))) begin
			`uvm_error(get_type_name(), "Cannot connect to interface vif")
		end
        endfunction: build_phase

	// Connect Phase        
        function void connect_phase(uvm_phase phase);
                super.connect_phase(phase);
                `uvm_info("DRIVER CLASS", "Connect Phase for Driver...", UVM_HIGH)
        endfunction: connect_phase

   	task reset_phase(uvm_phase phase);
        	`uvm_info("DRIVER_CLASS", "Reset Task", UVM_HIGH);
        	phase.raise_objection(this);
      		@(posedge vif.clk);
            	vif.reset <= 0;
            	#50;
            	vif.reset <= 1;
			#25;
        	phase.drop_objection(this);
    
	endtask: reset_phase

	// Run phase
        task run_phase(uvm_phase phase);
                super.run_phase(phase);
		item = FIR_sequence_item::type_id::create("item", this);
                `uvm_info("DRIVER CLASS", "Run Phase for Driver...", UVM_HIGH)
		
		forever begin
			// Receive the next packet from the sequencer
			seq_item_port.get_next_item(item);

			// Drive signals to item
			drive(item);

			// Item is finished
			seq_item_port.item_done();	
			
		end

        endtask: run_phase

	// Drive task pushes signals from packet to interface
	task drive(FIR_sequence_item item);
		
		@(posedge vif.clk);
		vif.reset <= item.reset;
		vif.inData <= item.inData;
		
	endtask: drive

endclass