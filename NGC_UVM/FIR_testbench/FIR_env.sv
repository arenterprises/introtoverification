
class FIR_env extends uvm_env;

	// Macro utility
	`uvm_component_utils(FIR_env)

	// Declaration of agent, scoreboard, and coverage classes
	FIR_agent agent;
	FIR_scoreboard scoreboard;
	FIR_cov cov;

	// SHA-256 Environment Constructor
	function new(string name="FIR_env", uvm_component parent);
		super.new(name, parent);
		`uvm_info("ENV CLASS", "Constructor...", UVM_HIGH)
	endfunction : new

	function void build_phase(uvm_phase phase);

		super.build_phase(phase);
		`uvm_info("ENV CLASS", "Entering Build Phase...", UVM_HIGH)
		agent = FIR_agent::type_id::create("agent", this);
		scoreboard = FIR_scoreboard::type_id::create("scoreboard", this);
		cov = FIR_cov::type_id::create("cov", this);
	endfunction : build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("ENV CLASS", "Entering Connect Phase...", UVM_HIGH)
		// Connect the monitor analysis port to the scoreboard port
		agent.monitor.mon_analysis_port.connect(scoreboard.scoreboard_port);

		// Connecting analysis port to coverage subscriber port
		agent.monitor.mon_analysis_port.connect(cov.analysis_export);
	endfunction : connect_phase

endclass : FIR_env
