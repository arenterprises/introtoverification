
interface FIR_interface (input bit clk);

	// Inputs	
	bit [31:0] inData;
	bit reset;
	// Outputs
	bit [31:0] outData;

endinterface: FIR_interface