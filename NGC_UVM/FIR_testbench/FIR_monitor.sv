
class FIR_monitor extends uvm_monitor;
	// Macro utility for automation
	`uvm_component_utils(FIR_monitor)

	// Declare a handle for the interface
	virtual FIR_interface vif;
	FIR_sequence_item packet;


	// Create the analysis port
	uvm_analysis_port #(FIR_sequence_item) mon_analysis_port;
	// Monitor constructor
	function new(string name="FIR_monitor", uvm_component parent);

		super.new(name, parent);
		`uvm_info("MONITOR CLASS", "Constructor...", UVM_HIGH)

	endfunction : new


	function void build_phase(uvm_phase phase);

		super.build_phase(phase);

		`uvm_info("MONITOR CLASS", "Entering Build Phase...", UVM_HIGH)
		// Create the instance for the analysis port
		mon_analysis_port = new("mon_analysis_port", this);
		// Configure the interface
		if(!uvm_config_db #(virtual FIR_interface)::get(this, "*", "vif", vif)) begin
			`uvm_error(get_type_name(), "Interface cannot be resolved")	
		end

	endfunction : build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("MONITOR CLASS", "Entering Connect Phase...", UVM_HIGH)
	endfunction : connect_phase
	

	// Run phase
	task run_phase(uvm_phase phase);

		super.run_phase(phase);		
		`uvm_info("MONITOR CLASS", "Entering Run Phase...", UVM_HIGH)

		forever begin
			packet = FIR_sequence_item::type_id::create("packet");

			wait(vif.reset != 0);
			// At the rising edge of the clock, pass signals
			// from the interface into the monitor packet
			@(posedge vif.clk);
			#100;
			packet.reset = vif.reset;
			packet.inData = vif.inData;

			@(posedge vif.clk);
			packet.outData = vif.outData;

			// Send packets to the scoreboard
			mon_analysis_port.write(packet);	
		end
		

	endtask : run_phase

endclass : FIR_monitor
