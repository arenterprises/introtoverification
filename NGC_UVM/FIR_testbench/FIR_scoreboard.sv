// Scoreboard Class for FIR Filter
// Alberto Reategui

class FIR_scoreboard extends uvm_scoreboard;
	// Macro utility for automation
	`uvm_component_utils(FIR_scoreboard)

	// Implement a scoreboard port
	uvm_analysis_imp #(FIR_sequence_item, FIR_scoreboard) scoreboard_port;
	// Create a FIFO queue to hold our transactions
	FIR_sequence_item transactions[$];

	// Scoreboard constructor
	function new(string name="FIR_scoreboard", uvm_component parent);
		super.new(name, parent);
		`uvm_info("SCOREBOARD CLASS", "Constructor...", UVM_HIGH);
	endfunction : new

	// Build_phase method
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		`uvm_info("SCOREBOARD CLASS", "Entering Build Phase...", UVM_HIGH);
		// Create the scoreboard port
		scoreboard_port = new("scoreboard_port", this);
	endfunction : build_phase

	// Connect_phase method
	function void connect_phase(uvm_phase phase);
	
		// Calling the super command
		super.connect_phase(phase);			
		`uvm_info("SCOREBOARD CLASS", "Entering Connect Phase...", UVM_HIGH);
	endfunction : connect_phase

	// Write task
	function void write(FIR_sequence_item packet);
		// Call a FIFO queue
		transactions.push_back(packet);
	endfunction : write

	// Run_phase task
	task run_phase(uvm_phase phase);

		super.run_phase(phase);
		`uvm_info("SCOREBOARD CLASS", "Entering Run Phase...", UVM_HIGH);

		forever begin
			
			// Receive the packet
			FIR_sequence_item cur_packet;
			if(transactions.size() != 0) begin
				cur_packet = transactions.pop_front();
				// Generate the expected value
				// Compare it with the actual value
				compare(cur_packet);
				// Score the transactions accordingly
			end
			#10;
		end		

	endtask : run_phase

	// Compare task - compare the expected and acutal values for each transaction
	task compare(FIR_sequence_item packet);

		// Create variables for the expected and actual values
		bit [39:0] expected_outData;
		bit [31:0] actual_outData = packet.outData;


		// Create variables for the computing function
		bit unsigned [31:0] coefficients [11] = '{ default : 32'h0 };
		bit [39:0] stepOutputs [11] = '{ default : 32'h0 };
		bit [31:0] firOut [11] = '{ default : 32'h0 };
	
		// Call the function
		expected_outData = computeOut(packet.inData, coefficients, stepOutputs, firOut);

		// Compare values and print out corresponding message
		if(actual_outData != expected_outData) begin
			`uvm_error("FALSE", $sformatf("Transaction invalid! EXP = %d ACT = %d ", expected_outData, actual_outData))
		end
		else begin
            		`uvm_info("TRUE", $sformatf("Transaction valid! EXP = %d ACT = %d ", expected_outData, actual_outData), UVM_LOW)
        	end 
	
	endtask : compare

	// Function computes the output of the FIR Filter
	function bit [31:0] computeOut( 
					input bit [31:0] inputData, 
					input bit unsigned [31:0] b [11], 
					input bit [39:0] Y [11], 
					input bit [31:0] firOut [11]
					);
		
		// Array will hold all of the coefficients
		b = '{ default : 32'h0 };
 
		b[0] = 32'h00000000;
		b[1] = 32'h00000004;
		b[2] = 32'h00000016;
		b[3] = 32'h00000044;
		b[4] = 32'h00000088;
		b[5] = 32'h000000bf;
		b[6] = 32'h000000bf;
		b[7] = 32'h00000088;
		b[8] = 32'h00000044;
		b[9] = 32'h00000016;
		b[10] = 32'h00000004;

		// Generate the multiplication steps
		Y = '{default: 32'h0};
		Y[0] = inputData*b[0];
		Y[1] = inputData*b[1];
		Y[2] = inputData*b[2];
		Y[3] = inputData*b[3];
		Y[4] = inputData*b[4];
		Y[5] = inputData*b[5];
		Y[6] = inputData*b[6];
		Y[7] = inputData*b[7];
		Y[8] = inputData*b[8];
		Y[9] = inputData*b[9];
		Y[10] = inputData*b[10];
		
		// Generate the outputs at each stage
		firOut = '{ default : 32'h0 };
		firOut[0] = Y[0] / 256;
		firOut[1] = Y[1] / 256;
		firOut[2] = Y[2] / 256;
		firOut[3] = Y[3] / 256;
		firOut[4] = Y[4] / 256;
		firOut[5] = Y[5] / 256;
		firOut[6] = Y[6] / 256;
		firOut[7] = Y[7] / 256;
		firOut[8] = Y[8] / 256;
		firOut[9] = Y[9] / 256;
		firOut[10] = Y[10] / 256;

		computeOut = (firOut[0] + firOut[1] + firOut[2] + firOut[3] + firOut[4] + firOut[5] + firOut[6] + firOut[7] + firOut[8] + firOut[9] + firOut[10]) / 2;

	endfunction


endclass : FIR_scoreboard
