

class FIR_sequence extends uvm_sequence;

	// Macro utility for automation
	`uvm_object_utils(FIR_sequence)
	
	FIR_sequence_item rst_pkt;
	FIR_sequence_item dereset_pkt;
	// Component constructor
	function new(string name="FIR_sequence");
		// Calling the super function
		super.new(name);
		`uvm_info("BASE SEQUENCE", "Constructor...", UVM_HIGH)
	endfunction : new

	task body();
		`uvm_info("BASE SEQUENCE", "Entering Base Sequence...", UVM_HIGH)
		rst_pkt = FIR_sequence_item::type_id::create("rst_pkt");
		// Start, randomize the data and finish sending the packet
		start_item(rst_pkt);
		rst_pkt.reset = 1'b0;
		finish_item(rst_pkt);
		#50;

		// Dereset the packet so the next packet can be created
		dereset_pkt = FIR_sequence_item::type_id::create("dereset_pkt");
		start_item(dereset_pkt);
		dereset_pkt.reset = 1'b1;
		finish_item(dereset_pkt);

	endtask : body	

endclass : FIR_sequence

class FIR_sequence_uno extends FIR_sequence;

	// Macro utility for automation
	`uvm_object_utils(FIR_sequence_uno)

	FIR_sequence_item packet;

	function new(string name="FIR_sequence_uno");
		super.new(name);
		`uvm_info("TEST SEQUENCE UNO", "Constructor...", UVM_HIGH)	
	endfunction : new


	task body();
		`uvm_info("TEST SEQUENCE UNO", "Entering Sequence One...", UVM_HIGH)
		packet = FIR_sequence_item::type_id::create("packet");
		
		start_item(packet);
		//packet.post_randomize();
		packet.inData = 32'haaaaaaaa;
		packet.reset = 1'b1;
		finish_item(packet);


	endtask : body

endclass : FIR_sequence_uno

class FIR_sequence_two extends FIR_sequence;

	// Macro utility for automation
	`uvm_object_utils(FIR_sequence_two)

	FIR_sequence_item packet;

	function new(string name="FIR_sequence_two");
		super.new(name);
		`uvm_info("TEST SEQUENCE TWO", "Constructor...", UVM_HIGH)	
	endfunction : new


	task body();
		`uvm_info("TEST SEQUENCE TWO", "Entering Sequence Two...", UVM_HIGH)
		packet = FIR_sequence_item::type_id::create("packet");
		
		start_item(packet);
		packet.post_randomize();
		//packet.inData = 32'h55555555;
		packet.reset = 1'b1;
		finish_item(packet);


	endtask : body

endclass : FIR_sequence_two

class FIR_sequence_three extends FIR_sequence;

	// Macro utility for automation
	`uvm_object_utils(FIR_sequence_three)

	FIR_sequence_item packet;

	function new(string name="FIR_sequence_three");
		super.new(name);
		`uvm_info("TEST SEQUENCE THREE", "Constructor...", UVM_HIGH)	
	endfunction : new


	task body();
		`uvm_info("TEST SEQUENCE THREE", "Entering Sequence THREE...", UVM_HIGH)
		packet = FIR_sequence_item::type_id::create("packet");
		
		start_item(packet);
		//packet.post_randomize();
		packet.inData = 32'h00000273;
		packet.reset = 1'b1;
		finish_item(packet);


	endtask : body

endclass : FIR_sequence_three 

