
class FIR_sequence_item extends uvm_sequence_item;

	// Inputs
	rand bit [31:0] inData;
	rand bit reset;
	// Output
	bit [31:0] outData;

	// Macro utility
	`uvm_object_utils(FIR_sequence_item)

	// Constructor
	function new(string name = "FIR_sequence_item");
		// Calling the super function
		super.new(name);
	endfunction: new

	// Range of valid values for input
	int unsigned minVal = 0;
	int unsigned maxVal = 2147483647 / 12;

	// Randomize function generates the random inputs
	function void post_randomize();

		// Generating the random input
		inData = $urandom_range(minVal, maxVal);

	endfunction : post_randomize

endclass: FIR_sequence_item
