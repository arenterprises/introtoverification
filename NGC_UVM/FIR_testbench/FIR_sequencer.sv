
class FIR_sequencer extends uvm_sequencer #(FIR_sequence_item);

        `uvm_component_utils(FIR_sequencer)
        
        function new(string name = "FIR_sequencer", uvm_component parent);
                super.new(name, parent);
                `uvm_info("SEQUENCER CLASS", "Constructor for Sequencer...", UVM_HIGH)
        endfunction: new

	// Build Phase        
        function void build_phase(uvm_phase phase);
                super.build_phase(phase);
		`uvm_info("SEQUENCER CLASS", "Build Phase for Sequencer...", UVM_HIGH)
        endfunction: build_phase

	// Connect Phase        
        function void connect_phase(uvm_phase phase);
                super.connect_phase(phase);
                `uvm_info("SEQUENCER CLASS", "Connect Phase for Sequencer...", UVM_HIGH)
        endfunction: connect_phase

	// Run phase
        task run_phase(uvm_phase phase);
                super.run_phase(phase);
                `uvm_info("SEQUENCER CLASS", "Run Phase for Sequencer...", UVM_HIGH)
        endtask: run_phase

endclass