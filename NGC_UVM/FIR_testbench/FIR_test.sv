
class FIR_base_test extends uvm_test;

	// Macro utility for automation
	`uvm_component_utils(FIR_base_test)

	// Create a handle for the SHA-256 Testbench environment
	FIR_env env;
	FIR_sequence rst_seq;
	FIR_sequence_uno test_seq;
	FIR_sequence_two test_seq2;
	FIR_sequence_three test_seq3;
	// FIR Base test constructor
	function new(string name="FIR_base_test", uvm_component parent);
		super.new(name, parent);
	endfunction : new

	// Build_phase method
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		`uvm_info("TEST CLASS", "Entering Build Phase...", UVM_HIGH)
		env = FIR_env::type_id::create("env", this);
	endfunction : build_phase

	// End_of_elaboration_phase
	function void end_of_elaboration_phase(uvm_phase phase);
		uvm_top.print_topology();
	endfunction : end_of_elaboration_phase

	// Connect_phase method
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("TEST CLASS", "Entering Connect Phase...", UVM_HIGH)               
	endfunction : connect_phase

	task run_phase(uvm_phase phase);
		super.run_phase(phase);
		`uvm_info("TEST CLASS", "Entering Run Phase...", UVM_HIGH)
		phase.raise_objection(this);
		
		// Reset Sequence
		//rst_seq = FIR_sequence::type_id::create("rst_seq");
		//rst_seq.start(env.agent.sequencer);
		//#100;

		// Test sequence
		repeat(10000) begin
			rst_seq = FIR_sequence::type_id::create("rst_seq");
			rst_seq.start(env.agent.sequencer);
			#100;

			//test_seq = FIR_sequence_uno::type_id::create("test_seq");
			//test_seq.start(env.agent.sequencer);
			//#10000;

			test_seq2 = FIR_sequence_two::type_id::create("test_seq2");
			test_seq2.start(env.agent.sequencer);
			#3500; 

			//test_seq3 = FIR_sequence_three::type_id::create("test_seq3");
			//test_seq3.start(env.agent.sequencer);
			//#10000;
		end
		phase.drop_objection(this);
	endtask : run_phase
endclass : FIR_base_test
