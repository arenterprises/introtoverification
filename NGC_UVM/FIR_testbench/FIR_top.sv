// FIR top module

import uvm_pkg::*;
`include "uvm_macros.svh"
`include "FIR_interface.sv"
`include "FIR_sequence_item.sv"
`include "FIR_sequence.sv"
`include "FIR_sequencer.sv"
`include "FIR_driver.sv"
`include "FIR_monitor.sv"
`include "FIR_cov.sv"
`include "FIR_agent.sv"
`include "FIR_scoreboard.sv"
`include "FIR_env.sv"
`include "FIR_test.sv"

module FIR_top;

	// Declare a handle for the clock bit
	bit clk;

	// Instantiate the interface
	FIR_interface intf(.clk(clk));

	FIR_filter dut(
		.inData(intf.inData),
		.clk(intf.clk),
		.reset(intf.reset),
		.outData(intf.outData)
	);

	// Setting the interface
	initial begin
		uvm_config_db #(virtual FIR_interface)::set(null, "*", "vif", intf);
	end

	// Start the test
	initial begin
	       run_test("FIR_base_test");
	end	

	// Clock generation
	initial begin

		clk = 0;
		#5;

		forever begin
			clk = ~clk;
			#2;
		end
			
	end

endmodule: FIR_top