package NGC_pkg;

    import uvm_pkg::*;

    `include "NGC_sequencer.sv"
    `include "NGC_monitor.sv"
    `include "NGC_driver.sv"
    `include "NGC_agent.sv"
    `include "NGC_scoreboard.sv"
    `include "NGC_config.sv"
    `include "NGC_env.sv"
    `include "NGC_test.sv"

endpackage: NGC_pkg