// aes agent class
    
class aes_192_agent extends uvm_agent;
`uvm_component_utils(aes_192_agent)
     
    aes_192_driver driver;
	aes_192_monitor monitor;
	aes_192_sequencer sequencer;


    // Constructor
    function new(string name = "aes_192_agent", uvm_component parent);
        super.new(name, parent);
        `uvm_info("AGENT_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    // Build phase
    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        `uvm_info("AGENT_CLASS", "Build Phase", UVM_HIGH);
        sequencer = aes_192_sequencer::type_id::create("sequencer", this);
		driver = aes_192_driver::type_id::create("driver", this);
		monitor = aes_192_monitor::type_id::create("monitor", this);
    endfunction: build_phase

    // Connect phase
    function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        `uvm_info("AGENT_CLASS", "Connect Phase", UVM_HIGH);
        driver.seq_item_port.connect(sequencer.seq_item_export);
    endfunction: connect_phase

    // Run phase
    task run_phase(uvm_phase phase);
        super.run_phase(phase);

    endtask: run_phase
    
endclass: aes_192_agent