// AES driver class
    
class aes_192_driver extends uvm_driver#(aes_192_packet);
    `uvm_component_utils(aes_192_driver)
    virtual aes_192_interface vif;

    // Constructor
    function new(string name = "aes_192_driver", uvm_component parent);
        super.new(name, parent);
        `uvm_info("DRIVER_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    // Build phase
    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        `uvm_info("DRIVER_CLASS", "Build Phase", UVM_HIGH);
        if(!(uvm_config_db #(virtual aes_192_interface)::get(this, "*", "vif", vif))) begin
            `uvm_error("DRIVER_CLASS", "Failed to get VIF from config DB")
        end
    endfunction: build_phase

    // Connect phase
    function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        `uvm_info("DRIVER_CLASS", "Connect Phase", UVM_HIGH);
    endfunction: connect_phase

    // Run phase
    task run_phase(uvm_phase phase);
        super.run_phase(phase);

    endtask: run_phase
    
endclass: aes_192_driver