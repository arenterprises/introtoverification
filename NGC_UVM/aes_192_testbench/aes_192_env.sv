// aes_192 environment class
    
class aes_192_env extends uvm_env;
    `uvm_component_utils(aes_192_env)

    aes_192_agent agent;
	aes_192_scoreboard scoreboard;

    // Constructor
    function new(string name = "aes_192_env", uvm_component parent);
        super.new(name, parent);
        `uvm_info("ENV_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    // Build phase
    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        `uvm_info("ENV_CLASS", "Build Phase", UVM_HIGH);
        agent = aes_192_agent::type_id::create("agent", this);
		scoreboard = aes_192_scoreboard::type_id::create("scoreboard", this);

    endfunction: build_phase

    // Connect phase
    function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        `uvm_info("ENV_CLASS", "Connect Phase", UVM_HIGH);
        agent.monitor.mon_analysis_port.connect(scoreboard.scoreboard_port);
    endfunction: connect_phase

    // Run phase
    task run_phase(uvm_phase phase);
        super.run_phase(phase);

    endtask: run_phase
    
endclass: aes_192_env