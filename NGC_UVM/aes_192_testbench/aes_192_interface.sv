// aes_192 Interface

interface aes_192_interface(input logic clk);

    input wire          rst; 
    input wire          start;
    input wire [127:0]  state;
    input wire [191:0]  key;
    output wire [127:0] out;
    output wire         out_valid;

endinterface: aes_192_interface