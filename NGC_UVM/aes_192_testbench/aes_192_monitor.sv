// aes_192 monitor class
    
class aes_192_monitor extends uvm_monitor;
    `uvm_component_utils(aes_192_monitor)
    virtual aes_192_interface vif;
    virtual aes_192_packet packet;
// Create the analysis port
	uvm_analysis_port #(aes_192_packet) mon_analysis_port;

    // Constructor
    function new(string name = "aes_192_monitor", uvm_component parent);
        super.new(name, parent);
        `uvm_info("MONITOR_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    // Build phase
    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        `uvm_info("MONITOR_CLASS", "Build Phase", UVM_HIGH);
        // Create the instance for the analysis port
		mon_analysis_port = new("mon_analysis_port", this);

        if(!(uvm_config_db #(virtual aes_192_interface)::get(this, "*", "vif", vif))) begin
            `uvm_error("MONITOR_CLASS", "Failed to get VIF from config DB")
        end
    endfunction: build_phase

    // Connect phase
    function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        `uvm_info("MONITOR_CLASS", "Connect Phase", UVM_HIGH);
    endfunction: connect_phase

    // Run phase
    task run_phase(uvm_phase phase);
        super.run_phase(phase);
        `uvm_info("MONITOR CLASS", "Entering Run Phase...", UVM_HIGH);

		forever begin
			packet = aes_192_packet::type_id::create("packet");

			// Wait for there to be no reset
			wait(!vif.rst);
			
			// At the rising edge of the clock, pass signals
			// from the interface into the monitor packet
			@(posedge vif.clk);
			packet.init = vif.init;
			packet.block = vif.block;
			packet.next = vif.next;

			@(posedge vif.clk);
			packet.ready = vif.ready;
			packet.digest = vif.digest;
			packet.digest_valid = vif.digest_valid;

			// Send packets to the scoreboard
			mon_analysis_port.write(packet);	
		end
		

    endtask: run_phase
    
endclass: aes_192_monitor