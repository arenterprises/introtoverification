
class aes_192_packet extends uvm_sequence_item;

	// Inputs
	//rand logic		clk;
	rand logic		rst;
	rand logic		start;
	rand logic		[127:0] state;
	rand logic		[191:0] key; 

	// Outputs
	logic		[127:0] out;
	logic		out_valid;
	//logic 		end_key;
	
	// Macro Utility automation 
	`uvm_object_utils_begin(aes_192_packet)
		`uvm_field_int(clk, UVM_DEFAULT)
		`uvm_field_int(rst, UVM_DEFAULT)
		`uvm_field_int(start, UVM_DEFAULT)
		`uvm_field_int(state, UVM_DEFAULT)
		`uvm_field_int(key, UVM_DEFAULT)
		`uvm_field_int(out, UVM_DEFAULT)
		`uvm_field_int(out_valid, UVM_DEFAULT)
	`uvm_object_utils_end

	// AES-192 Packet Constructor
	function new(string name="aes_192_packet");
		super.new(name);
	endfunction : new

	// Packet randomization function
	function void aes_192_randomize();
		//send random packets of info into the driver->interface
		int key_length = $urandom_range(0, 128);
		int state_length = (128 - key_length) % 192;
		bit [63:0] key_length_bits = key_length;


        // Set random values for the message
        foreach (msg_original[i]) begin
            if (i < msg_length)
                msg_original[i] = $urandom();
        end
        
        msg_padded = msg_original;

        // Add padding bit 1 followed by zeroes
        msg_padded[msg_length] = 1'b1;
        for (int i = msg_length + 1; i < msg_length + padding_length; i++) begin
            msg_padded[i] = 1'b0;
        end

        // Add message length as 64-bit value at end of padded message
        msg_padded[191:128] = msg_length_bits;



	endfunction : aes_192_randomize

endclass : aes_192_packet