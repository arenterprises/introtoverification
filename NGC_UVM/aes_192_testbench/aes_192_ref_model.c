#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <svdpi.h>
#include <openssl/aes.h>

void aes_192_ref_model(const svBitVecVal *inputBlock, int blockSize, svOpenArrayHandle expectedDigest){

  char block[192];
  char buffer[192];
  // Convert the input from logic type to string
  char inputString[blockSize + 1];
  // Loop to convert each value to string
  for(int i = 0; i < blockSize; i++){
    inputString[i] = (char) svGetBit(&inputBlock[blockSize-1-i], 0);
  }

  // Construct command to run aes_192 sum-gets string from aes
  // Here needs to be the end output of the aes-192 after key generation and mashing
  //snprintf(block, sizeof(block), "echo -n '%s' | openssl aes-192-cbc > 'encrypted file'", inputBlock);
  snprintf(block, sizeof(block), "echo -n '%s' | openssl aes-192-cbc ", inputBlock);
  // Open the block
  FILE* fp = popen(block, "r");
  // Store the contents of the block into the buffer
  fgets(buffer, sizeOf(buffer), fp);
  // MUST: Close the block
  pclose(fp);

  // Parse aes_192 Hash from output takes string as last 64 bytes(all chars)
  char aes_192_hash[64+1];
  sscanf(buffer, "%64s", aes_192_hash);

  // Convert aes_192 hash(chars) to bit vector
  for(int i = 0; i < 64; i++){
    int index = 64 - 1 - i;
    svPutBit(&expectedDigest, i, aes_192_hash[index/2] >> (index % 2) & 0x1);
  }
  
}