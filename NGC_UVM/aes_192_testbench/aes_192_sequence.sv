
class aes_192_sequence extends uvm_sequence;
    `uvm_object_utils(aes_192_sequence)
    aes_192_sequence_item reset_pkt;

    // Constructor
    function new(string name = "aes_192_sequence");
        super.new(name);
        `uvm_info("SEQUENCE_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    task body();
        `uvm_info("SEQUENCE_CLASS", "Body task", UVM_HIGH);
        reset_pkt = aes_192_sequence_item::type_id::create("reset_pkt");
        start_item(reset_pkt);
        reset_pkt.randomize() with {reset == 1;};
        finish_item(reset_pkt);
    endtask: body

endclass: aes_192_sequence

class aes_192_test1_sequence extends aes_192_sequence;
    `uvm_object_utils(aes_192_test1_sequence)
    aes_192_sequence_item test1_pkt;

    // Constructor
    function new(string name = "aes_192_test1_sequence");
        super.new(name);
        `uvm_info("TEST1_SEQUENCE_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    task body();
        `uvm_info("TEST1_SEQUENCE_CLASS", "Body task", UVM_HIGH);
        test1_pkt = aes_192_sequence_item::type_id::create("test1_pkt");
        start_item(test1_pkt);
        test1_pkt.randomize() with {test1_pkt == 0;};
        finish_item(test1_pkt);
    endtask: body

endclass: aes_192_test1_sequence
