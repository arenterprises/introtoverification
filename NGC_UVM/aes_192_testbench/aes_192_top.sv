// AES Encryption Top Module
import uvm_pkg::*;
`include "uvm_macros.svh"
`include "aes_192_interface.sv"
`include "aes_192_sequence_item.sv"
`include "aes_192_sequence.sv"
`include "aes_192_sequencer.sv"
`include "aes_192_driver.sv"
`include "aes_192_monitor.sv"
`include "aes_192_agent.sv"
`include "aes_192_scoreboard.sv"
`include "aes_192_env.sv"
`include "aes_192_test.sv"

module aes_192_top;
    logic clk;

    // Instantiate interface
    aes_192_interface intf(.clk(clk));

    // Connect interface to DUT
    aes_192 dut(
        .clk(intf.clk),
        .rst(intf.rst),
        .init(intf.init),
        .msg_padded(intf.msg_padded),
        .msg_in_valid(intf.msg_in_valid),
        .msg_output(intf.msg_output),
        .msg_out_valid(intf.msg_out_valid),
        .ready(intf.ready)
    );

    // Interface settings
    initial begin
        uvm_config_db #(virtual aes_192_interface)::set(null, "*", "vif", intf);
    end

    // Start test
    initial begin
        run_test();
    end

    // Clock generation
    initial begin
        clk = 0;
        #5;
        forever begin
            clk = ~clk;
            #2;
        end
    end

endmodule: aes_192_top
