// MD5 agent class
    
class md5_agent extends uvm_agent;
`uvm_component_utils(md5_agent)

    // Constructor
    function new(string name = "md5_agent", uvm_component parent);
        super.new(name, parent);
        `uvm_info("AGENT_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    // Build phase
    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        `uvm_info("AGENT_CLASS", "Build Phase", UVM_HIGH);
    endfunction: build_phase

    // Connect phase
    function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        `uvm_info("AGENT_CLASS", "Connect Phase", UVM_HIGH);
    endfunction: connect_phase

    // Run phase
    task run_phase(uvm_phase phase);
        super.run_phase(phase);

    endtask: run_phase
    
endclass: md5_agent