// MD5 Interface

interface md5_interface(input logic clk);

    logic         rst;
    logic         init;
    logic [511:0] msg_padded;
    logic         msg_in_valid;
    logic [127:0] msg_output;
    logic         msg_out_valid;
    logic         ready;

endinterface: md5_interface