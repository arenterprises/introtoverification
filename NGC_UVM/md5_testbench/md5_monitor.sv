// MD5 monitor class
    
class md5_monitor extends uvm_monitor;
    `uvm_component_utils(md5_monitor)
    virtual md5_interface vif;

    // Constructor
    function new(string name = "md5_monitor", uvm_component parent);
        super.new(name, parent);
        `uvm_info("MONITOR_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    // Build phase
    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        `uvm_info("MONITOR_CLASS", "Build Phase", UVM_HIGH);
        if(!(uvm_config_db #(virtual md5_interface)::get(this, "*", "vif", vif))) begin
            `uvm_error("MONITOR_CLASS", "Failed to get VIF from config DB")
        end
    endfunction: build_phase

    // Connect phase
    function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        `uvm_info("MONITOR_CLASS", "Connect Phase", UVM_HIGH);
    endfunction: connect_phase

    // Run phase
    task run_phase(uvm_phase phase);
        super.run_phase(phase);

    endtask: run_phase
    
endclass: md5_monitor