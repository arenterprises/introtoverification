
class md5_sequence extends uvm_sequence;
    `uvm_object_utils(md5_sequence)
    md5_sequence_item reset_pkt;

    // Constructor
    function new(string name = "md5_sequence");
        super.new(name);
        `uvm_info("SEQUENCE_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    task body();
        `uvm_info("SEQUENCE_CLASS", "Body task", UVM_HIGH);
        reset_pkt = md5_sequence_item::type_id::create("reset_pkt");
        start_item(reset_pkt);
        reset_pkt.randomize() with {reset == 1;};
        finish_item(reset_pkt);
    endtask: body

endclass: md5_sequence

class md5_test1_sequence extends md5_sequence;
    `uvm_object_utils(md5_test1_sequence)
    md5_sequence_item test1_pkt;

    // Constructor
    function new(string name = "md5_test1_sequence");
        super.new(name);
        `uvm_info("TEST1_SEQUENCE_CLASS", "Constructor", UVM_HIGH);
    endfunction: new

    task body();
        `uvm_info("TEST1_SEQUENCE_CLASS", "Body task", UVM_HIGH);
        test1_pkt = md5_sequence_item::type_id::create("test1_pkt");
        start_item(test1_pkt);
        test1_pkt.randomize() with {test1_pkt == 0;};
        finish_item(test1_pkt);
    endtask: body

endclass: md5_test1_sequence
