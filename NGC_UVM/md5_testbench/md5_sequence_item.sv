// MD5 sequence item class

class md5_sequence_item extends uvm_sequence_item;
    `uvm_object_utils(md5_sequence_item)

    // Sequence properties
    rand bit [511:0] msg_padded;
    bit msg_in_valid;

    function new(string name = "md5_sequence_item");
        super.new(name);
    endfunction: new

endclass: md5_sequence_item