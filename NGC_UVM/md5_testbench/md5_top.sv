// MD5 Encryption Top Module
import uvm_pkg::*;
`include "uvm_macros.svh"
`include "md5_interface.sv"
`include "md5_sequence_item.sv"
`include "md5_sequence.sv"
`include "md5_sequencer.sv"
`include "md5_driver.sv"
`include "md5_monitor.sv"
`include "md5_agent.sv"
`include "md5_scoreboard.sv"
`include "md5_env.sv"
`include "md5_test.sv"

module md5_top;
    logic clk;

    // Instantiate interface
    md5_interface intf(.clk(clk));

    // Connect interface to DUT
    md5 dut(
        .clk(intf.clk),
        .rst(intf.rst),
        .init(intf.init),
        .msg_padded(intf.msg_padded),
        .msg_in_valid(intf.msg_in_valid),
        .msg_output(intf.msg_output),
        .msg_out_valid(intf.msg_out_valid),
        .ready(intf.ready)
    );

    // Interface settings
    initial begin
        uvm_config_db #(virtual md5_interface)::set(null, "*", "vif", intf);
    end

    // Start test
    initial begin
        run_test();
    end

    // Clock generation
    initial begin
        clk = 0;
        #5;
        forever begin
            clk = ~clk;
            #2;
        end
    end

endmodule: md5_top
