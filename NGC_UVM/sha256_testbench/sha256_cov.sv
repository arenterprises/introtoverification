// Subscriber class for Functional Coverage
// Alberto Reategui

class sha256_cov extends uvm_subscriber #(sha256_packet);

	`uvm_component_utils(sha256_cov)

	// Variable for coverage reported in the module
	real cov;

	// inputs
	bit [63:0] pkt_length_bits = 0;
	int pkt_length;

	covergroup input_msg;
		// Denotes every instance a message of a certain length occurs
		option.per_instance = 1;
		coverpoint pkt_length {

			bins length = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55};

		}

	endgroup : input_msg

	// Coverage constructor
	function new(string name = "sha256_cov", uvm_component parent);
	
		super.new(name, parent);
		// Create an instance of the covergroup
		input_msg = new();

	endfunction : new

	// Write method
	function void write(T t);

		// Extract the length of the input message in bits and bytes
		pkt_length_bits = t.block[63:0];
		pkt_length = pkt_length_bits/8;

		// Sample the input message
		input_msg.sample();

	endfunction
    // Extract phase
	function void extract_phase(uvm_phase phase);

		// Get the coverage from the input message
		cov = input_msg.get_coverage();

	endfunction : extract_phase

    // Report phase
	function void report_phase(uvm_phase phase);

		// Report the result of the test
		`uvm_info(get_full_name(), $sformatf("Coverage is %d", cov), UVM_NONE);

	endfunction : report_phase
	

endclass : sha256_cov