// SHA-256 Driver Class
// Alberto Reategui

class sha256_driver extends uvm_driver #(sha256_packet);

	// Macro utility for automation
	`uvm_component_utils(sha256_driver)

	// Create a handle for the interface and data packet
	virtual sha256_interface vif;
	sha256_packet packet;
	// Driver constructor
	function new(string name="sha256_driver", uvm_component parent);
		super.new(name, parent);
		`uvm_info("DRIVER CLASS", "Constructor...", UVM_HIGH)
	endfunction : new

	// Creating the build_phase method	
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		`uvm_info("DRIVER CLASS", "Entering Build Phase...", UVM_HIGH)
		// Check if the interface has been connected
		if(!(uvm_config_db #(virtual sha256_interface)::get(this, "*", "vif", vif))) begin
			`uvm_error(get_type_name(), "Interface vif could not be obtained")
		end
	endfunction : build_phase

	function void connect_phase(uvm_phase phase);
	
		// Calling the super command
		super.connect_phase(phase);			
		`uvm_info("DRIVER CLASS", "Entering Connect Phase...", UVM_HIGH)
	endfunction : connect_phase

	// Reset phase
   	task reset_phase(uvm_phase phase);
        	`uvm_info("DRIVER_CLASS", "Reset Task", UVM_HIGH);
        	phase.raise_objection(this);
      		// Insert delays as well as reseting test sequences
			@(posedge vif.clk);
			vif.rst <= 1;
			vif.init <= 0;
            #50;
			// Deassert reset value and initialize data
			vif.rst <= 0;
			vif.init <= 1;
			#25;
        	phase.drop_objection(this);
    
	endtask: reset_phase


	// Creating the run_phase task
	task run_phase(uvm_phase phase);
		
		super.run_phase(phase);

		// Create instance of packet
		packet = sha256_packet::type_id::create("packet", this);

		`uvm_info("DRIVER CLASS", "Entering Run Phase...", UVM_HIGH)

		forever begin
			// Receive the next packet from the sequencer 
			seq_item_port.get_next_item(packet);


			// Drive signals to interface
			drive_packet(packet);

			// Check to see if packet is sent
			seq_item_port.item_done();
			   
			
		end

	endtask : run_phase


	// Creating the drive_packet task to drive the signals
	task drive_packet(sha256_packet packet);

      	@(posedge vif.clk);
		vif.rst <= packet.rst;
		vif.init <= packet.init;
		vif.block <= packet.block;
		vif.next <= packet.next;
		
	endtask : drive_packet

endclass : sha256_driver