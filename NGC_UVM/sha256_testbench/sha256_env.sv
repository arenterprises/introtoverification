// Testbench Environment Class
// Alberto Reategui
class sha256_env extends uvm_env;

	// Macro utility
	`uvm_component_utils(sha256_env)

	// Agent and declaration
	sha256_agent agent;
	sha256_scoreboard scoreboard;
	sha256_cov cov;

	// SHA-256 Environment Constructor
	function new(string name="sha256_env", uvm_component parent);
		super.new(name, parent);
		`uvm_info("ENV CLASS", "Constructor...", UVM_HIGH)
	endfunction : new

	// Build phase
	function void build_phase(uvm_phase phase);

		super.build_phase(phase);
		`uvm_info("ENV CLASS", "Entering Build Phase...", UVM_HIGH)
		// Create instances of agent, scoreboard and coverage classes
		agent = sha256_agent::type_id::create("agent", this);
		scoreboard = sha256_scoreboard::type_id::create("scoreboard", this);
		cov = sha256_cov::type_id::create("cov", this);
	endfunction : build_phase

	// Connect phase
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("ENV CLASS", "Entering Connect Phase...", UVM_HIGH)
		
		// Connecting monitor analysis port to scoreboard port
		agent.monitor.mon_analysis_port.connect(scoreboard.scoreboard_port);

		// Connecting analysis port to coverage subscriber port 
		agent.monitor.mon_analysis_port.connect(cov.analysis_export);

	endfunction : connect_phase

endclass : sha256_env
