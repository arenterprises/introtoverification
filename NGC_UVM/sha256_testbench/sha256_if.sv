interface sha256_if (input logic clock);

	// Inputs
	logic		clk;
	logic		rst;
	logic		init;
	logic		next;
	logic		[511:0] block;
	// Outputs
	logic		ready;
	logic		[255:0] digest;
	logic		digest_valid;

endinterface : sha256_if