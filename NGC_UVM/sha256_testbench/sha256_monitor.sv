// Monitor class
// Alberto Reategui

class sha256_monitor extends uvm_monitor;
	// Macro utility for automation
	`uvm_component_utils(sha256_monitor)

	// Declare a handle for the interface
	virtual sha256_interface vif;
	sha256_packet packet;
	// Create the analysis port
	uvm_analysis_port #(sha256_packet) mon_analysis_port;
	// Monitor constructor
	function new(string name="sha256_monitor", uvm_component parent);

		super.new(name, parent);
		`uvm_info("MONITOR CLASS", "Constructor...", UVM_HIGH)

	endfunction : new

	// Build Phase
	function void build_phase(uvm_phase phase);

		super.build_phase(phase);

		`uvm_info("MONITOR CLASS", "Entering Build Phase...", UVM_HIGH)
		// Create the instance for the analysis port
		mon_analysis_port = new("mon_analysis_port", this);
		// Configurate the interface
		if(!uvm_config_db #(virtual sha256_interface)::get(this, "*", "vif", vif)) begin
			`uvm_error(get_type_name(), "Interface cannot be resolved")	
		end

	endfunction : build_phase

	// Connect Phase
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("MONITOR CLASS", "Entering Connect Phase...", UVM_HIGH)

	endfunction : connect_phase
	

	// Run phase
	task run_phase(uvm_phase phase);

		super.run_phase(phase);		
		`uvm_info("MONITOR CLASS", "Entering Run Phase...", UVM_HIGH)

		forever begin
			
			#2000;
			packet = sha256_packet::type_id::create("packet", this);
			// At the rising edge of the clock, pass signals
			// from the interface into the monitor packet
          		@(vif.digest_valid);
			
			packet.init = vif.init;
			packet.block = vif.block;
			packet.next = vif.next;
			
			@(posedge vif.clk);
			packet.digest = vif.digest;
			packet.digest_valid = vif.digest_valid;
			packet.ready = vif.ready;

			// Send packets to the scoreboard
			mon_analysis_port.write(packet);	
		    
		
		end

	endtask : run_phase

endclass : sha256_monitor
