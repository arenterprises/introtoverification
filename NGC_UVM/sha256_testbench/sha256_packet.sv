// Packet class
// Alberto Reategui

class sha256_packet extends uvm_sequence_item;

	// Inputs
	bit		rst;
	bit		init;
	bit		next;
	bit	[511:0] block;
	// Outputs
	bit		ready;
	bit		[255:0] digest;
	bit		digest_valid;

	// Integer values for character range
	int maxASCII = 122;
	int minASCII = 48;

	// Macro utility for automation
	`uvm_object_utils(sha256_packet)

	// Create a block 
	bit [511:0] sha_block;

	int bitIndex = 511;

	// Constructor for the packet
	function new(string name="sha256_packet");
		super.new(name);
	endfunction : new

	// Randomize the packet values
	function void post_randomize();
		// Declare a handle for the size of the packet in bits
		bit [63:0] pkt_length_bits;
		// Create integer variables to store the packet size and padding
		int pkt_length = $urandom_range(0, 55);
		int padding_length = (448-(pkt_length*8)) % 512;

		// Store length of the message in big endian format
		pkt_length_bits = pkt_length*8;
		

		for(int i = 0; i < pkt_length; i++) begin

			sha_block[bitIndex -: 8] = $urandom_range(minASCII, maxASCII);
			bitIndex -= 8;

		end

		// Set the block equal to the randomly generated block
		block = sha_block;
		// Set the bit equal to the pkt_length to 1
		block[511-(pkt_length*8)] = 1'b1;


		// For each bit after the pkt_length bit, pad with zeros
		for(int i = 64; i < 511-(pkt_length*8); i++) begin
			block[i] = 1'b0;
		end
		
		

		// Set the remaining 64 bits to be the length of the block in bits 
		block[63:0] = pkt_length_bits;

		
	endfunction : post_randomize


endclass : sha256_packet