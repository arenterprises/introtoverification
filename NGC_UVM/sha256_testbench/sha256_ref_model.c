#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <svdpi.h>
#include <math.h>

void sha256_ref_model(const svOpenArrayHandle inputBlock, int msgLength, svOpenArrayHandle expectedDigest){

	char block[1024];
	char buffer[1024];

  unsigned char inputString[64] = {0};

  for(int i = msgLength-1; i >= 0; i--) {
  
    // Select the bit at index i and store into bit variable
    int bit = svGetBitselBit(inputBlock, i);
    
    // Calculate index to place bit into input string
    int char_arr_index = (msgLength-1-i)/8;
    
    // Calculate bit index
    int char_bit_index = 7 - ((msgLength-1-i) % 8);
    
    
    // Place in correct bit index
    inputString[char_arr_index] |= (bit << char_bit_index);
  
    //printf("%d", bit);
  
  }
  
  //printf("\n");
  
  // Create a character array (String) for the message block length
  unsigned char msgLenString[8] = {0};
  
  // Loop through the last 8 bytes where the length is stored
  for(int i = 63; i >= 56; i--) {
  
    msgLenString[63-i] = inputString[i];
  
  }
  
  int length = 0;
  
  // Compute the length of the last 8 bytes
  for(int i = 0; i < 8; i++) {
  
      length += (msgLenString[i] * pow(256, i));
    
  }
  
  //printf("%d \n", length);
  
  // Stores the number of bytes the length of the string is
  int numBytes = length/8;
  
  // Stores our input message 
  unsigned char blockMessage[numBytes+1];
  
  // Set 
  for(int i = 0; i < numBytes; i++) {
  
    blockMessage[i] = inputString[i];
  
  }
  
  blockMessage[numBytes] = '\0';
  
  // A POSIX compatible string
  unsigned char commandString[2*numBytes + 1];
  
  // Index variable to account for escape characters
  int index = 0;
  // Create the string to enter into command
  for(int i = 0; i < numBytes; i++) {
  
    if(blockMessage[i] < 48 ||  (blockMessage[i] > 57 && blockMessage[i] < 65) || (blockMessage[i] > 91 && blockMessage[i] < 97) || blockMessage[i] > 122) {
    
      commandString[index++] = 92;
    
    }
  
    commandString[index++] = blockMessage[i];
  
  }
  
  commandString[index] = '\0';
  
  
  //printf("Command String: %s \n", commandString);
   
	// Construct command to run sha256sum
	snprintf(block, sizeof(block), "echo -n %s | sha256sum", commandString);
	// Open the block
	FILE* fp = popen(block, "r");
	// Store the contents of the block into the buffer
	fgets(buffer, sizeof(buffer), fp);
	// MUST: Close the block
	pclose(fp);

	// Parse SHA256 Hash from output
	char sha256_hash[64];
	sscanf(buffer, "%64s", sha256_hash);

  //printf("%64s in the C code", sha256_hash);

/* 
  // Create an integer array
  int sha256_int_hash[32]; 
 

  // Convert
  for(int k = 0; k < 32; k++){
    sha256_int_hash[k] = sha256_hash[k];
  } 
  

	// Convert SHA-256 hash to bit vector
	for(int i = 0; i < 32; i++){
    for(int j = 0; j < 8; j++){
      svBit bit = (sha256_int_hash[32-1-i] >> j) & (0x01);
      svPutBitselBit((svBitVecVal*)expectedDigest, j+8*i, bit);
      
    }
    
	}
*/

  // Convert hash characters to appropriate binary values
  for(int k = 0; k < 64; k++) {
  
    int binVal = 0;
    char c  = sha256_hash[k];
    
    if(c >= '0' && c <= '9') {
    
      binVal = c - '0';
    
    }
    else if( c >= 'a' && c <= 'f') {
    
      binVal = c - 'a' + 10;
    
    }
    else if(c >= 'A' && c <= 'F') {
    
      binVal = c - 'A' + 10;
    
    }
    else {
    
      binVal = 0;
    
    }
  
    
    // Place bits in the correct position
    for(int l = 0; l < 4; l++) {
    
      int position = (k*4) + l;
      
      svBit bit = (binVal >> (3-l)) & 1;
      
      svPutBitselBit(expectedDigest, 255-position, bit);
    
    }
    
 
  }
 
}


