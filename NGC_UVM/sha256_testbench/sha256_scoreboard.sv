// Scoreboard class
// Alberto Reategui

// Importing the C language reference model
import "DPI-C" context function void sha256_ref_model(input bit [511:0] inputBlock, input int blockLength, output bit [255:0] outputBlock);

class sha256_scoreboard extends uvm_scoreboard;
	// Macro utility for automation
	`uvm_component_utils(sha256_scoreboard)

	// Implement a scoreboard port
	uvm_analysis_imp #(sha256_packet, sha256_scoreboard) scoreboard_port;
	// Create a FIFO queue to hold our transactions
	sha256_packet transactions[$];

	// Scoreboard constructor
	function new(string name="sha256_scoreboard", uvm_component parent);
		super.new(name, parent);
		`uvm_info("SCOREBOARD CLASS", "Constructor...", UVM_HIGH);
	endfunction : new

	// Build_phase method
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		`uvm_info("SCOREBOARD CLASS", "Entering Build Phase...", UVM_HIGH);
		// Create the scoreboard port
		scoreboard_port = new("scoreboard_port", this);
	endfunction : build_phase

	// Connect_phase method
	function void connect_phase(uvm_phase phase);
	
		// Calling the super command
		super.connect_phase(phase);			
		`uvm_info("SCOREBOARD CLASS", "Entering Connect Phase...", UVM_HIGH);
	endfunction : connect_phase

	// Write task
	function void write(sha256_packet packet);
		// Call a FIFO queue
		transactions.push_back(packet);
	endfunction : write

	// Run_phase task
	task run_phase(uvm_phase phase);

		super.run_phase(phase);
		`uvm_info("SCOREBOARD CLASS", "Entering Run Phase...", UVM_HIGH);

		forever begin
			
			// Receive the packet
			sha256_packet cur_packet;
			wait(transactions.size() != 0);
			cur_packet = transactions.pop_front();
			// Generate the expected value
			// Compare it with the actual value
			compare(cur_packet);
			// Score the transactions accordingly
			
		end		

	endtask : run_phase

	// Compare task - compare the expected and acutal values for each transaction

	task compare(sha256_packet packet);

		// Create variables for the expected and actual values
		bit [255:0] expected_digest;
		bit [255:0] actual_digest = packet.digest;
		
		// Call the reference model 		
		sha256_ref_model(packet.block, $bits(packet.block), expected_digest);	

		// Compare values and print out corresponding message
		if(actual_digest != expected_digest) begin
			`uvm_error("FALSE", $sformatf("Transaction invalid! EXP = %h ACT = %h ", expected_digest, actual_digest))
		end
		else begin
            		`uvm_info("TRUE", $sformatf("Transaction valid! EXP = %h ACT = %h ", expected_digest, actual_digest), UVM_LOW)
        end 
	
	endtask : compare

endclass : sha256_scoreboard
