// Sequence Class
// Alberto Reategui
class sha256_sequence extends uvm_sequence;

	// Macro utility for automation
	`uvm_object_utils(sha256_sequence)
	// Declare handle for reset packet
	sha256_packet rst_pkt;

	// Component constructor
	function new(string name="sha256_sequence");
		// Calling the super function
		super.new(name);
		`uvm_info("BASE SEQUENCE", "Constructor...", UVM_HIGH)
	endfunction : new

	task body();
		`uvm_info("BASE SEQUENCE", "Entering Base Sequence...", UVM_HIGH)
		rst_pkt = sha256_packet::type_id::create("rst_pkt");
		// Start, randomize the data and finish sending the packet
		start_item(rst_pkt);
		rst_pkt.rst = 1;
		rst_pkt.next = 1;		
		finish_item(rst_pkt);

	endtask : body	

endclass : sha256_sequence

// Empty string sequence
class sha_sequence_uno extends sha256_sequence;

	// Macro utility for automation
	`uvm_object_utils(sha_sequence_uno)

	sha256_packet packet;

	function new(string name="sha_sequence_uno");
		super.new(name);
		`uvm_info("TEST SEQUENCE UNO", "Constructor...", UVM_HIGH)	
	endfunction : new


	task body();
		`uvm_info("TEST SEQUENCE UNO", "Entering Sequence One...", UVM_HIGH)
		packet = sha256_packet::type_id::create("packet");
		
		start_item(packet);
		packet.block = 512'b10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
		packet.init = 1'b1;
		packet.next = 1'b1;
		finish_item(packet);


	endtask : body

endclass : sha_sequence_uno

class sha_sequence_dos extends sha256_sequence;

	// Macro utility for automation
	`uvm_object_utils(sha_sequence_dos)

	sha256_packet packet;

	function new(string name="sha_sequence_dos");
		super.new(name);
		`uvm_info("TEST SEQUENCE DOS", "Constructor...", UVM_HIGH)	
	endfunction : new


	task body();
		`uvm_info("TEST SEQUENCE DOS", "Entering Sequence Two...", UVM_HIGH)
		packet = sha256_packet::type_id::create("packet");
		
		start_item(packet);
		// Generate randomized packet with designated values for inputs
		packet.post_randomize();
		packet.rst = 1'b0; 
		packet.init = 1'b1;
		packet.next = 1'b1;
		finish_item(packet);


	endtask : body
endclass: sha_sequence_dos

// Message of maximum length according to design
class sha_full_length_sequence extends sha256_sequence;

	// Macro utility for automation
	`uvm_object_utils(sha_full_length_sequence)

	sha256_packet packet;

	function new(string name="sha_full_length_sequence");
		super.new(name);
		`uvm_info("TEST SEQUENCE THREE", "Constructor...", UVM_HIGH)	
	endfunction : new


	task body();
		`uvm_info("TEST SEQUENCE THREE", "Entering Sequence Three...", UVM_HIGH)
		packet = sha256_packet::type_id::create("packet");
		
		start_item(packet);
		packet.block = 512'b01010010011000010110111001100100011011110110110100100000011100110111010001110010011010010110111001100111011100110010000001100001011100100110010100100000011011100110111101110100001000000111010001101000011001010010000001100110011101010111010001110101011100100110010100101100001000000110100101110011001000000111010001101000011000010111010000100000011000110110111101110010011100100110010101100011011101000011111100100000010010000111010101101000100000000000000000000000000000000000000000000000000000000000000110111000;
		//packet.post_randomize();
		packet.rst = 1'b0; 
		packet.init = 1'b1;
		packet.next = 1'b1;
		finish_item(packet);


	endtask : body	

endclass : sha_full_length_sequence
