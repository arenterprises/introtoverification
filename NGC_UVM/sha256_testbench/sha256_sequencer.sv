// Sequencer Class
// Alberto Reategui

class sha256_sequencer extends uvm_sequencer #(sha256_packet);
	
	// Declare handle for the sha256 data packet 
	sha256_packet packet;	
	
	// Macro utility for automation
	`uvm_component_utils(sha256_sequencer)

	// Component constructor
	function new(string name="sha256_sequencer", uvm_component parent);
		// Calling the super function
		super.new(name, parent);
		`uvm_info("SEQUENCER CLASS", "Constructor...", UVM_HIGH);
	endfunction : new

	// Creating the build_phase
	function void build_phase(uvm_phase phase);
		// Calling the super command
		super.build_phase(phase);
		`uvm_info("SEQUENCER CLASS", "Entering Build Phase...", UVM_HIGH);		
	endfunction : build_phase

	// Creating the connect_phase
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("SEQUENCER CLASS", "Entering Connect Phase...", UVM_HIGH);
	endfunction

endclass : sha256_sequencer