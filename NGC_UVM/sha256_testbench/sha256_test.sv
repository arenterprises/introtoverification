// Test Class
// Alberto Reategui

class sha256_base_test extends uvm_test;

	// Macro utility for automation
	`uvm_component_utils(sha256_base_test)

	// Create a handle for the SHA-256 Testbench environment
	sha256_env env;
	sha256_sequence rst_seq;
	sha256_sequence test_seq;
	sha256_sequence test_seq_2;
	sha256_sequence test_seq_3;
	// SHA-256 Base test constructor
	function new(string name="sha256_base_test", uvm_component parent);
		super.new(name, parent);
	endfunction : new

	// Build_phase method
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		`uvm_info("TEST CLASS", "Entering Build Phase...", UVM_HIGH)
		env = sha256_env::type_id::create("env", this);
	endfunction : build_phase

	// End_of_elaboration_phase
	function void end_of_elaboration_phase(uvm_phase phase);
		uvm_top.print_topology();
	endfunction : end_of_elaboration_phase

	// Connect_phase method
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		`uvm_info("TEST CLASS", "Entering Connect Phase...", UVM_HIGH)               
	endfunction : connect_phase

	task run_phase(uvm_phase phase);
		super.run_phase(phase);
		`uvm_info("TEST CLASS", "Entering Run Phase...", UVM_HIGH)
		phase.raise_objection(this);
		
		// Test sequence
		repeat(10000) begin
			// Reset Sequence
			rst_seq = sha256_sequence::type_id::create("rst_seq");
			rst_seq.start(env.agent.sequencer);
			#100;

			// Randomized string sequence
			test_seq_2 = sha_sequence_dos::type_id::create("test_seq_2");
			test_seq_2.start(env.agent.sequencer);
			#2500;

		end
		phase.drop_objection(this);
	endtask : run_phase
endclass : sha256_base_test


