// Top level module
// Alberto Reategui


import uvm_pkg::*;
`include "uvm_macros.svh"
`include "sha256_if.sv"
`include "sha256_packet.sv"
`include "sha256_sequence.sv"
`include "sha256_sequencer.sv"
`include "sha256_driver.sv"
`include "sha256_monitor.sv"
`include "sha256_cov.sv"
`include "sha256_agent.sv"
`include "sha256_scoreboard.sv"
`include "sha256_env.sv"
`include "sha256_test.sv"

module sha256_top;
// Instantiate the clock signal
	bit clk;

	// Instantiate the interface
	sha256_interface intf(.clk(clk));
	
	// Create an instance of the sha256_dut
	sha256 dut (
		// Connecting our inputs and outputs from the interface
		// to the DUT
		.clk(intf.clk),
		.rst(intf.rst),
		.init(intf.init),
		.next(intf.next),
		.block(intf.block),
		.ready(intf.ready),
		.digest(intf.digest),
		.digest_valid(intf.digest_valid)
		
	);


	// Setting the interface
	initial begin
		uvm_config_db #(virtual sha256_interface)::set(null, "*", "vif", intf);
	end

	// Start the test
	initial begin
	       run_test("sha256_base_test");
	end

	// Clock generation
	initial begin

		clk = 0;
		#5;

		forever begin
			clk = ~clk;
			#2;
		end
			
	end
endmodule
